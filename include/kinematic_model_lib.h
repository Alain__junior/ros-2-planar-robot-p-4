#pragma once
#include "eigen3/Eigen/Dense"

// Kinematic model of a planar 2DoF serial manipulator
class RobotModel{
  public:
    // Create object representing a kinematic model of a 2DoF serial manipulator with lengths `l1` and `l2`
    RobotModel(const double &l1, const double &l2);
    // Computes the cartesian position `x` and jacobian matrix `J` for a joint position `q`
    void FwdKin(Eigen::Vector2d &x, Eigen::Matrix2d &J, const Eigen::Vector2d & q);
  private:
    const double l1   ;
    const double l2   ;
};