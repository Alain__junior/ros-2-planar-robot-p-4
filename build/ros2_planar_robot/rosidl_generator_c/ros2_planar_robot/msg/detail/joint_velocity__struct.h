// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ros2_planar_robot:msg/JointVelocity.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__STRUCT_H_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

/// Struct defined in msg/JointVelocity in the package ros2_planar_robot.
typedef struct ros2_planar_robot__msg__JointVelocity
{
  double dq[2];
} ros2_planar_robot__msg__JointVelocity;

// Struct for a sequence of ros2_planar_robot__msg__JointVelocity.
typedef struct ros2_planar_robot__msg__JointVelocity__Sequence
{
  ros2_planar_robot__msg__JointVelocity * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ros2_planar_robot__msg__JointVelocity__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__STRUCT_H_
