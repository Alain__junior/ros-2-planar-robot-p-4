// generated from rosidl_generator_c/resource/idl.h.em
// with input from ros2_planar_robot:msg/KinData.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__KIN_DATA_H_
#define ROS2_PLANAR_ROBOT__MSG__KIN_DATA_H_

#include "ros2_planar_robot/msg/detail/kin_data__struct.h"
#include "ros2_planar_robot/msg/detail/kin_data__functions.h"
#include "ros2_planar_robot/msg/detail/kin_data__type_support.h"

#endif  // ROS2_PLANAR_ROBOT__MSG__KIN_DATA_H_
