# ros2_planar_robot

## November $24^{th}$ 2023

### Intermediary Goal (1h)

- Based on [this](https://docs.ros.org/en/foxy/Tutorials/Intermediate/Launch/Creating-Launch-Files.html) tutorial, write a launch file `ros2_planar_robot_launch.py` in a folder `launch` that initiates the nodes `trajectory_generator`, `controller`, `kinematic_model`, `simulator` and `disturbance_generator`.

- The launch file should set the parameters `l1` and `l2`, as described in [this](https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Using-Parameters-In-A-Class-CPP.html) tutorial.

- The whole system should work properly whe the following commands are performed

    - terminal 1: `ros2 launch ros2_planar_robot ros2_planar_robot_launch.py`
    - terminal 2: `ros2 run ros2_planar_robot high_lev_mng`  &rarr;   enter trajectory ref_poses
    - terminal 3: `ros2 topic pub -1 /launch std_msgs/msg/Bool "{data: true}"`

### Final Assignment - commit due December 1st @23h59

- The current commit includes the following files

    - `urdf/planar_robot.urdf.xml`: urdf description of a 2 DoF robot with $l_1 = l_2 = 1.0$ m.
    - `urdf/planar_robot.rviz`: configuration rviz file 
    - `launch/simple_visualizer.py`: launches a `tf` static broadcaster and a `robot_state_publisher`.

- Necessary tutorials:

    - [Introducing `tf2`](https://docs.ros.org/en/foxy/Tutorials/Intermediate/Tf2/Introduction-To-Tf2.html)
    - Section "The proper way to publish static transform" of [`tf2` static broadcaster tutorial](https://docs.ros.org/en/foxy/Tutorials/Intermediate/Tf2/Writing-A-Tf2-Static-Broadcaster-Cpp.html#the-proper-way-to-publish-static-transforms)
    - [Using URDF with `robot_state_publisher`](https://docs.ros.org/en/foxy/Tutorials/Intermediate/URDF/Using-URDF-with-Robot-State-Publisher.html)

- In order to visualize a robot with joint positions $\{q_0,q_1\}$, as described in [this tutorial](https://docs.ros.org/en/foxy/Tutorials/Intermediate/URDF/Using-URDF-with-Robot-State-Publisher.html), four things are necessary:
    - launch rviz with the configuration `urdf/planar_robot.rviz`, using
    ```
    rviz2 -d install/ros2_planar_robot/share/ros2_planar_robot/urdf/planar_robot.rviz
    ```
    - launch a `robot_state_publisher`, in charge of publishing the coordinate frames of each link.

    - Broadcast a coordinate frame `odom`, with child frame `axis`

    - Publish $\{q_0,q_1\}$ in the topic `/joint_states` with type `JointStates`.


- **Hint** &rarr; In order to create a message of type `JointState` in C++, the following lines of code can be used:
```
auto message  = sensor_msgs::msg::JointState();
rclcpp::Time now = this->get_clock()->now();
message.header.stamp = now;
message.name = {"joint_name_1","joint_name_2"}; // Check joint names in the urdf file
message.position = {q0,q1};
```

### Main Goal:

Develop a node `joint_pub` that publishes joint positions in the `/joint_states` topic based on the `/q` topic, such that the robot motion is visualized in rviz.

------------------------------------------------------------------------------------

### Answers

#### 1. Explain what the nodes launched in `launch/simple_visualizer.py` file do.

#### 2. Write here the instructions to demonstrate the functionality of the developed solutions:



------------------------------------------------------------------------------------

### Updating your forked directory

You can update your forked repository adding this project as a remote, fetching the updated commits and merging it with your current local:

```
cd ros_workspace/src/ros2_planar_robot
git remote add prof git@gitlab.com:joao.cv/ros2_planar_robot.git
git fetch assignment_remote
git merge assignment_remote/main
```
You will probably need to solve the conflicts, selecting the desired changes between your local and this remote. You can use `vscode` for that.

------------------------------------------------------------------------------------