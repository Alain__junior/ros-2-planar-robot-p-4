from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='ros2_planar_robot',
            namespace='ros2_planar_robot',
            executable='trajectory_generator',
            name='ros2_planar1'
        ),
        Node(
            package='ros2_planar_robot',
            namespace='ros2_planar_robot',
            executable='simulator',
            name='ros2_planar2'
        ),
        Node(
            package='ros2_planar_robot',
            namespace='ros2_planar_robot',
            
            executable='kinematic_modeller',
            output="screen",
            emulate_tty=True,
            parameters=[
                {"l1": 0.0 , "l2": 0.0}
            ],
            name='ros2_planar3'
        ),
        Node(
            package='ros2_planar_robot',
            namespace='ros2_planar_robot',
            executable='disturbance_generator',
            emulate_tty=True,
            parameters=[
                {"A": 0.4 , "omega": 3.0}
            ],
            name='ros2_planar5'
        ),
        Node(
            package='ros2_planar_robot',
            namespace='ros2_planar_robot',
            executable='controller',
            name='ros2_planar'
        )
        
    ])