// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ros2_planar_robot:msg/JointVelocity.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__STRUCT_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__STRUCT_HPP_

#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

#include "rosidl_runtime_cpp/bounded_vector.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


#ifndef _WIN32
# define DEPRECATED__ros2_planar_robot__msg__JointVelocity __attribute__((deprecated))
#else
# define DEPRECATED__ros2_planar_robot__msg__JointVelocity __declspec(deprecated)
#endif

namespace ros2_planar_robot
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct JointVelocity_
{
  using Type = JointVelocity_<ContainerAllocator>;

  explicit JointVelocity_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      std::fill<typename std::array<double, 2>::iterator, double>(this->dq.begin(), this->dq.end(), 0.0);
    }
  }

  explicit JointVelocity_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  : dq(_alloc)
  {
    if (rosidl_runtime_cpp::MessageInitialization::ALL == _init ||
      rosidl_runtime_cpp::MessageInitialization::ZERO == _init)
    {
      std::fill<typename std::array<double, 2>::iterator, double>(this->dq.begin(), this->dq.end(), 0.0);
    }
  }

  // field types and members
  using _dq_type =
    std::array<double, 2>;
  _dq_type dq;

  // setters for named parameter idiom
  Type & set__dq(
    const std::array<double, 2> & _arg)
  {
    this->dq = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ros2_planar_robot::msg::JointVelocity_<ContainerAllocator> *;
  using ConstRawPtr =
    const ros2_planar_robot::msg::JointVelocity_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ros2_planar_robot::msg::JointVelocity_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ros2_planar_robot::msg::JointVelocity_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::msg::JointVelocity_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::msg::JointVelocity_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::msg::JointVelocity_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::msg::JointVelocity_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ros2_planar_robot::msg::JointVelocity_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ros2_planar_robot::msg::JointVelocity_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ros2_planar_robot__msg__JointVelocity
    std::shared_ptr<ros2_planar_robot::msg::JointVelocity_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ros2_planar_robot__msg__JointVelocity
    std::shared_ptr<ros2_planar_robot::msg::JointVelocity_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const JointVelocity_ & other) const
  {
    if (this->dq != other.dq) {
      return false;
    }
    return true;
  }
  bool operator!=(const JointVelocity_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct JointVelocity_

// alias to use template instance with default allocator
using JointVelocity =
  ros2_planar_robot::msg::JointVelocity_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_VELOCITY__STRUCT_HPP_
